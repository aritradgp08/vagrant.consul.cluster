This is a programming exercise to build Redis and Consul clusters through Vagrant. A total of 5 VMs have been used.

The vagrantfile helps all the instances to communicate with each other. Here, 4 Consul instances have been used among which 3 are consul servers and the other acts as a client.

Pre - requisites for this exercise :

* Vagrant
* VirtualBox

In order to start the instances there is only one command that is required to be executed:

*vagrant up*

Environment Variables:

All the instances have been defined separately so that the size of the cluster as well as other environment variables or creating multiple nodes can be created and configured as and when required.

Due to limitations of having a machine which cannot support many VMs, redis sentinels could not be used.

Testing with Docker:

In order to smoke test the operation, Docker has been used.

In order to install Docker, please refer to https://docs.docker.com/engine/getstarted/step_one/#/docker-for-windows

The smoketest needs the external script to run : https://aritradgp08@bitbucket.org/aritradgp08/docker-rediscluster.git

It will set up a master/ slave configuration and 3 sentinel clusters for testing.